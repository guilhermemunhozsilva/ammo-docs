# Ammo Web


## Data Layer
Os metodos que são utilizador para popular o dataLayer estão localizados no `DataLayerService.ts`. Para a variável `dataLayer` funcionar localmante, é necessário commentar o conteúdo `API_ENV === 'production' &&` que consta no arquivo `_document.tsx` junto ao link(`https://apis.google.com/js/platform.js`) e ao script do google.


## Recomendação (PDP)
Para que o componente de recomendação receba os dados, é necessário ir no projeto `marilena-backend` no arquivo `GoogleRecommendationService.ts` dentro do método `getRecommendations` a uma variável `dryRun` que precisa ser colado o valor `false` para a requisição retornar os produtos recomendados.

##

# Marilena-backend

## [SKU](https://drive.google.com/file/d/1QTBzgxPSWiC0ErxqzYTba7SXMP0zivbU/view?usp=sharing) e [Bundle](https://drive.google.com/file/d/130soqxVXjQ0R6gVknsIdXT3g7klqeKv3/view?usp=sharing)
Os endpoints para subir os xlsx (arquivo excel) se encontram nos arquivos `BackOfficeSkusResource` api `Post - path /active/download`, `BackOfficeBundleResource` api `Post - path /`. Um bundle é um sku que também tem registro na tabela `bundle_items`, logo, para criar um bundle com os itens, é necessário que todos os skus tenham sido criados anteriormente.
Ao enviar o arquivo de SKU, a condição de criação de um produto novo é "não constar um product já existente com o mesmo item `agreggation`", que pode ser encontrado no exemplo do link de excel a cima.

# Banco de Dados

## Relacionamentos

### Coupon x Campaign

O range de funcionamento de um coupon pode ser definido de três formas, dentre elas: 
 - A coluna `distributorId` na tabela `coupon_codes`, podendo colocar o valor da loja/distrbuitor que irá receber o coupon.
 - Colocando o valor **false** na coluna `validOnlyForIssuerDistributor` da tabela `coupon_campaigns` que é relacionada ao coupon, fazendo com que ignore o campo na primeira opção.
 - Criando um range na tabela `distributor_2_lists`, onde relaciona o id de cada **distributor** a uma lista que é vinculada a campaign (tabela `coupon_campaigns`) na coluna `distrbutorIdList`

